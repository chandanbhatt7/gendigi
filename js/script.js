var genDen = (function () {

    var looper;

    //custom built counterUp
    var countUp = function () {
        $('#counter').waypoint({
            handler: function (direction) {
                $('.count').each(function () {
                    var $this = $(this),
                        countTo = $this.attr('to');
                    $({countNum: $this.text()}).animate({
                            countNum: countTo
                        },
                        {
                            duration: 2000,
                            easing: 'linear',
                            step: function () {
                                $this.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $this.text(this.countNum);
                            }
                        });
                });
            },
            offset: function () {
                return Waypoint.viewportHeight()
            }
        });
    };

    var expertiseOwlCarousel = $('.expertise-banner-carousel').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        dots: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    });

    var expertiseBackgroundOwlCarousel = $('.expertise-background-overlay-container').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        dots: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    })

    var typing = function (id, strings, callback, callback2) {
        new TypeIt(id, {
            strings: strings,
            loop: true,
            speed: 150,
            cursor: true,
            nextStringDelay: 1000,
            breakLines: false,
            beforeString: callback,
            beforeStep: callback2
        });

    }

    var scrollNavBar = function (prevScrollpos) {
        var currentScrollPos = window.pageYOffset;
        var navBar = $("#navbar");
        if (prevScrollpos > currentScrollPos) {
            navBar.css('top', '0');
        }
        else {
            navBar.css('top', '-90px');
        }
        return currentScrollPos;
    }

    var glowIcons = function (e) {
        setInterval(function () {

            $('.slide-2-svgslide').removeClass('slide-2-svgslide-shownow')
            var randomNo = [];
            randomNo[0] = Math.floor(((Math.random() * 100) + 1) % 35);
            randomNo[1] = Math.floor(((Math.random() * 100) + 1) % 35);
            randomNo[2] = Math.floor(((Math.random() * 100) + 1) % 35);
            randomNo[3] = Math.floor(((Math.random() * 100) + 1) % 35);
            randomNo[4] = Math.floor(((Math.random() * 100) + 1) % 35);
            randomNo.forEach(data => {
                $('.svg-' + data).addClass('slide-2-svgslide-shownow');
            });

        }, 1000)
    }

    var svgCount = function ($this) {

        var countTo = $this.attr('to');
        $this.text('0')
        $({countNum: $this.text()}).animate({
                countNum: countTo
            },
            {
                duration: 2000,
                easing: 'linear',
                step: function () {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function () {
                    $this.text(this.countNum);
                }
            });
    }

    var svgAnimation = function () {
        if (window.innerWidth < 992) {
            $('#svg-item1').show();
            var $svg = $("#svg1").drawsvg();
            $svg.drawsvg('animate');
            svgCount($("#svg1 + .svg-count-para span.svg-count"));
            var nItems = $('.svg-carousel > div').size();
            var iNextSlide = 2;
            var iCurrentSlide = 1;
            looper = setInterval(function () {
                if (iNextSlide > nItems) {
                    iCurrentSlide = 1;
                    iNextSlide = 1;
                }

                $('.svg-carousel > div').hide();
                $('.svg-carousel > #svg-item' + iNextSlide).show();
                $svg = $("#svg" + iNextSlide).drawsvg();
                svgCount($("#svg" + iNextSlide + " + .svg-count-para span.svg-count"));
                $svg.drawsvg('animate');

                iCurrentSlide = iNextSlide;
                iNextSlide++;
            }, 3000);
        }
        else {
            var $svg1 = $("#svg1").drawsvg();
            $svg1.drawsvg('animate');
            var $svg2 = $("#svg2").drawsvg();
            $svg2.drawsvg('animate');
            var $svg3 = $("#svg3").drawsvg();
            $svg3.drawsvg('animate');
            $('.svg-count').each(function () {
                var $this = $(this);
                svgCount($this);
            });
        }
    }

    var resetSvgCount = function () {
        if (window.innerWidth < 992) {
            window.clearInterval(looper);
        }
        else {
            $('.svg-count').each(function () {
                $(this)[0].innerText = 0;
            });
        }
    }

    var rescursiveBackground = function () {
        $('#scroll-img').animate({backgroundPositionY: '-=2px'}, 20, 'linear', rescursiveBackground);
    }

    var owlCarousel = function () {
        $(".owl-carousel").owlCarousel({
            items: 1,
            margin: 20,
            loop: true,
            center: true,
            autoplay: true,
            autoplayTimeout: 1500,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            dots: false,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false
        });
    };

    var animateScroll = function () {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }

    return {
        countUp: countUp,
        typing: typing,
        scrollNavBar: scrollNavBar,
        glowIcons: glowIcons,
        rescursiveBackground: rescursiveBackground,
        svgAnimation: svgAnimation,
        owlCarousel: owlCarousel,
        resetSvgCount: resetSvgCount,
        animateScroll: animateScroll,
        expertiseOwlCarousel: expertiseOwlCarousel,
        expertiseBackgroundOwlCarousel: expertiseBackgroundOwlCarousel

    }
})();


$(function () {

    genDen.countUp();

    genDen.glowIcons();
    var carousel = $("#myCarousel");

    // to count initial scroll position
    var prevScrollpos = window.pageYOffset;

    //for hiding and showing navbar on scroll
    $(window).scroll(function () {
        prevScrollpos = genDen.scrollNavBar(prevScrollpos);
    });

    var pushbar = new Pushbar({
        overlay: true,
    });
    //for handling pushbar
    $('#pushbar-btn').on('click', function () {
        pushbar.open('mypushbar');
    })

    var homeSlideTyping = '#slide-1-text';
    var homeSlideTypingText = [" stunning ", " scalable ", " innovative ", " intuitive ", " awesome ", " addictive "];
    var careerPageTyping = '#career-hiring-text';
    var careerPageTypingText = [" Android Geeks! ", " iOS Developers! ", " Server Gurus! ", " UI Developers! ", " Graphic Designers! "];
    var expertisePageTyping = '#banner-typed-text';
    var expertisePageTypingText = ["Design - UI & UX", "UI Frameworks", "mBaaS", "Location based Apps", "Android Applications"];

    // index page slide typing text
    if (homeSlideTyping[0]) {
        genDen.typing(homeSlideTyping, homeSlideTypingText, function (e) {
        }, function (e) {
        });
    }

    // career page slide typing text
    if (careerPageTyping[0]) {
        genDen.typing(careerPageTyping, careerPageTypingText, function (e) {
        }, function (e) {
        });
    }

    // expertise page slide typing text
    if (expertisePageTyping[0]) {
        genDen.typing(expertisePageTyping, expertisePageTypingText, function (e) {
            genDen.expertiseOwlCarousel.trigger('next.owl.carousel');
            genDen.expertiseBackgroundOwlCarousel.trigger('next.owl.carousel');
        }, function (e) {
            console.log(e)
        });
    }


    carousel.on('slid.bs.carousel', function () {
        var x = $(this).find('.active').index();
        switch (x) {
            case 0 :
                //for typing.js
                genDen.typing();
                break;
            case 1 :
                break;
            case 2 :
                break;
            case 3 :
                //svg animation
                genDen.svgAnimation();
                break;
            case 4 :
                break;
            case 5 :
                //recursive background image
                genDen.rescursiveBackground();
                //owl carousel
                genDen.owlCarousel();
                break;
        }
    });

    carousel.on('slide.bs.carousel', function () {
        var x = $(this).find('.active').index();
        switch (x) {
            case 0 :
                //for typing.js

                break;
            case 1 :
                break;
            case 2 :
                break;
            case 3 :
                //svg animation
                genDen.resetSvgCount();
                break;
            case 4 :
                break;
            case 5 :

                break;
        }
    });

    $('.faq-section-container a.faq-read-more-btn').on('click', function (data) {
        var faqId = $(data.currentTarget).data('id');
        $("#" + faqId).slideToggle();
        $("#" + faqId + " + .dots").toggle();
        if ($(data.currentTarget).html() === "Read more") {
            $(data.currentTarget).html("Read less");
        }
        else {
            $(data.currentTarget).html("Read more");
        }
    })

    $("#infographic-carousel").owlCarousel({
        loop: true,
        items: 3,
        autoplay: true,
        autoplayTimeout: 2000,
        nav: true,
        navText: ['<i class="fas fa-angle-left fa-2x"></i>', '<i class="fas fa-angle-right fa-2x"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            }
        }
    });

    var phoneOwlCarousel = $("#iphone-carousel");
    phoneOwlCarousel.owlCarousel({
        loop: true,
        items: 3,
        rtl: true,
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false,
        autoplay: true,
        autoplayTimeout: 3000,
        margin: 10,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    var textOwlCarousel = $("#iphone-text-carousel");
    textOwlCarousel.owlCarousel({
        loop: true,
        items: 1,
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false,
        dots: false,
        animateIn: 'slideInUp'
    });
    phoneOwlCarousel.on('changed.owl.carousel', function (event) {
        textOwlCarousel.trigger('next.owl.carousel');
    })


    $("#portfolio-carousel").owlCarousel({
        loop: true,
        items: 3,
        dots: false,
        margin: 20,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            }
        }
    });

    $("#whitepaper-carousel").owlCarousel({
        items: 3,
        nav: true,
        navText: ['<i class="fas fa-angle-left fa-2x"></i>', '<i class="fas fa-angle-right fa-2x"></i>'],
        margin: 10
    });


    $("#employee-view-carousel").owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        dots: false,
        center: true
    });

    $("#testimonial-carousel").owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false
    });

    $(".animate-scroll").on('click', genDen.animateScroll);
});